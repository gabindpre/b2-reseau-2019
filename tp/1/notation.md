name | note | I. NIC, DHCP | I. Table routage/ARP | I. Ports en écoute | I. DNS | I. FW | II. NIC edit | II. Bonus : Bonding | II. Serv SSH | II. WS : 3WHS + ARP | II. Bonus : WS : SSH | III. Routage Simple | IV. `iftop` + Cockpit + Netdata
--- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
lecoz | 13 | x | - | - | x | x | x | o | x | o | o | x | x |
TABBARA | 18 | + | x | x | x | + | x | x | x | x | x | + | x
thys | 15 | x | x | x | x | - | x | x | x | x | o | x | x 
giovannangeli | 7 | x | - | - | o | o | o | o | o | o | o | o | -
Papon, Trouvat Bouffartigues | 10 | x | - | o | - | o | x | o | o | o | o | o | o
Usereau | 11 | - | - | + | o | - | x | x | x | o | o | - | x | 
léveil | 14 | x | x | x | x | + | x | o | x | x | o | x | -
REGNAULT | 9 | x | - | - | o | o | x | o | x | o | o | o | o
Requena | 10 | x | - | o | o | o | x | x | x | o | o | o | x
larrieu | 14 | x | - | x | + | - | x | o | x | o | o | + | x
DIAS | 14 | x | - | - | x | x | x | o | + | o | o | + | x
mouret | 12 | - | o | x | o | x | x | o | x | x | o | o | x
feydit | 11 | + | x | x | x | o | + | o | x | o | o | o | o
FARGUES, Gillot, Menard | 13 | x | - | x | - | x | x | o | - | o | o | o | o
mouchague | 12 | - | o | x | x | x | x | x | - | o | o | x | x
DEMANECHE | 6 | x | x | - | o | o | o | - | - | o | o | o | o
aDURAND | 16 | x | x | x | x | x | x | o | - | o | o | + | x
boisseau | 9 | x | - | o | - | o | x | o | x | o | o | o | o
Marques | 10 | x | o | x | x | - | x | o | o | o | o | o | o
GIRARDEAU | 17 | x | x | x | x | x | x | x | x | x | x | x | x
Fourfooz | 12 | x | x | x | o | o | x | o | x | o | o | o | -
castera | 14 | x | o | x | x | - | x | x | x | x | o | x | x
FEUGERE | 14 | x | x | x | o | x | x | o | x | o | o | x | x
delas | 12 | x | o | x | o | - | x | o | x | o | o | o | o
boueix | 15 | x | x | - | x | x | x | o | x | x | - | o | x
delbrel | 8 | x | o | x | o | o | - | o | o | o | o | o | o
EDURAND | 13 | x | o | x | - | x | x | o | x | x | - | x | -
verrier | 10 | x | x | x | x | - | x | o | o | o | o | o | o
malbec | 10 | x | x | x | - | x | x | o | x | o | o | o | o
ceberio | 12 | x | x | x | - | x | x | o | x | x | o | o | o
vicente, salmi | 17 | x | x | x | x | x | x | o | x | x | x | x | x
dumont | 11 | x | x | o | - | - | x | o | x | o | o | o | o
CASELLES  | 13 | x | x | x | o | o | x | o | x | o | o | x | x
lenaour | 10 | x | - | o | x | - | x | o | x | o | o | o | o
salles | 13 | x | x | x | x | + | x | o | x | o | o | - | o
bernard | 12 | x | - | - | x | o | x | o | x | o | o | o | o
laborde | 15 | x | - | x | x | x | x | + | x | o | o | x | x
depaire | 13 | x | - | x | - | - | x | o | x | x | o | o | o
lacoste | 14 | x | - | o | - | x | x | x | x | x | o | x | x
VENDEE | 10 | x | - | x | x | x | x | o | - | - | o | o | o
aboulicam | 14 | x | x | x | - | + | x | o | x | o | o | x | -
PETIT | 11 | x | - | x | x | o | x | o | x | o | o | o | o
maury | 9 | x | x | x | x | - | x | o | o | o | o | o | o
